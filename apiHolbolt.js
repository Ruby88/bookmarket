export default {
    create : function(input, resolve){

       fetch('https://2f6niolswh.execute-api.us-east-1.amazonaws.com/dev/users/create', {
           method : 'POST',
           body : JSON.stringify(input)
       })
       .then(data=>{
           return data.json()
       }).then(realData=>{
           resolve(realData);
       })

    },
    login : function(input, resolve){

        fetch('https://2f6niolswh.execute-api.us-east-1.amazonaws.com/dev/users/login', {
            method : 'POST',
            body : JSON.stringify(input)
        })
        .then(data=>{
            return data.json()
        }).then(realData=>{
            console.warn(realData);
            resolve(realData);
        })
 
     },
     getUserData: function(username, resolve){
        fetch('https://2f6niolswh.execute-api.us-east-1.amazonaws.com/dev/users/get?username='+ username)
        .then(data=>{
            return data.json()
        }).then(realData=>{
            console.warn(realData);
            resolve(realData);
        })
     }
}