import React, { Component } from 'react';
import { ScrollView,View, SafeAreaView,Text, StyleSheet} from 'react-native';
import {Searchbar} from 'react-native-paper';
import Book from '../components/book';

export default class Itbooks extends Component {
  state ={
      word : '',
      books : []
  }

  componentWillMount(){
      fetch('https://api.itbook.store/1.0/new') .then(hariu=>{
        return hariu.json()
     })
     .then(json=>{
         console.warn(json);
         if (json.books)
         this.setState({books: json.books})
     })
  }

  render() {
      let {books} = this.state;
      const {navigation} = this.props;
    return <ScrollView style={styles.container}> 
         <Searchbar 
            placeholder="Хайх үгээ оруулна уу"
            onChangeText={word => { 
                if (word.length>=3){
                    fetch('https://api.itbook.store/1.0/search/' + word)
                     .then(hariu=>{
                        return hariu.json()
                     })
                     .then(json=>{
                         if (json.books)
                         this.setState({books: json.books})
                     })
                }
                
                this.setState({ word: word }); }}
           />
           {books.map((book,index)=> {
               book.thumbnailUrl = {uri : book.image};
               let book2 = [];
               if (books.length!=index+1) {
                book2 = books[index+1];
                book2.thumbnailUrl = {uri : book2.image};
               }
            
              return <View style={{flexDirection : 'row'}}>
                  <Book data={book} navigation={navigation}  style={{flex:1, alignItems: 'center',}} count={book.pageCount}   />
                  {book2.image? <Book data={book2} navigation={navigation}  style={{flex:1, alignItems: 'center',}} count={book.pageCount}   />
             : <Text></Text>}
             </View>
             })}
           

      </ScrollView>;
  }
}

Itbooks.navigationOptions = {
    title: 'IT Books',
  };
  

  const styles = StyleSheet.create({
      container : {
          flex : 1,
          paddingTop : 15,
          backgroundColor : '#f3f3f3',

      }

  })