import React, { Component } from 'react';
import { View, Text, StyleSheet,Image, KeyboardAvoidingView } from 'react-native';
import { Headline, Avatar,  Button, TextInput } from 'react-native-paper';
import api from '../apiHolbolt';

export default class UserScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username : '',
      password : ''
    };
  }

  render() {
    const { navigation} = this.props;
    let {username, password} = this.state
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
     
         <Image size={180} style={{borderRadius: 150}}  source={require('../assets/images/logo3.png')} />
         <View style={{height: 100, width: 300, flexDirection: 'column'}}>  
          <TextInput
              label='Нэр'
              mode='outlined'
              value={this.state.name}
              onChangeText={text => this.setState({ username : text})}
          />
              <TextInput
              label='Нууц үгээ оруулна уу'
              mode='outlined'
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={text => this.setState({ password : text })}
          />
       </View>
         <View style={styles.buttonContainer}>
     
            <Button icon="contacts" style={{margin : 8, height : 50, backgroundColor : "#337AFF", justifyContent : 'center'}} mode="contained" onPress={() => {
                 api.login({username, password}, hariu=>{
                   if(hariu.message==1) {
                    navigation.navigate('Main')
                   } else {
                     return alert(hariu.message)
                   }
                 })
                //  
            }}>
              Нэвтрэх
            </Button>
            <Button icon="contacts" style={{margin : 8, height : 50, backgroundColor : "#337AFF",  justifyContent : 'center',}} mode="contained" onPress={() => navigation.navigate('Register')}>
              Бүртгүүлэх
            </Button>
            </View>
      </KeyboardAvoidingView>
    );
  }
}

UserScreen.navigationOptions = {
  title: 'Profile',
};


 const styles = StyleSheet.create({
   container : {
     flex :1,
    //  backgroundColor : '#dcdcdc',
     justifyContent : 'center',
     alignItems : 'center'
   },
   buttonContainer : {
    textAlign : 'center',
    marginTop : 50,
    flexDirection : 'row'
   }
 });