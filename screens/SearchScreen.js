import React from 'react';
import { ScrollView, StyleSheet, Text , TextInput, Button, View} from 'react-native';
import Books from './LinkScreen';
import {Searchbar} from 'react-native-paper';
export default class SearchScreen extends React.Component {
 state={
   word : ''
 }

 render(){ 
  let {word} = this.state;

  return (
    <ScrollView style={styles.container}>
        <View style={{flexDirection: ("row")}}>
      <Searchbar style={{flex : 1}}
        placeholder="Хайх үгээ оруулна уу"
        onChangeText={word => { this.setState({ word: word }); }}
        
      />
        
        </View>
        
        <Books haihUg={word}></Books>

    </ScrollView>
  );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: '#fff',
  },
});
