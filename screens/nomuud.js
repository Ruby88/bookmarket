export default [
    {
      "title": "Upstream pre intermediate A1 student's book",
      "isbn": "978-1-84558-761-1",
      "pageCount": 416,
      "publishedDate": { "$date": "2008-04-01" },
      "thumbnailUrl": require('../assets/images/halzka.jpg'),
      "authors": "Virginia Evans - Jenny Dooley",
      "pdf" : "https://pdfnomuud.s3-us-west-2.amazonaws.com/book.pdf",
      "categories": "Англи хэл" 
    },

  {
    "title": "Upstream pre intermediate B1 student's book",
    "isbn": "978-1-84558-761-1",
    "pageCount": 592,
    "publishedDate": { "$date": "2011-01-14" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Virginia Evans - Jenny Dooley",
    "categories":  "Англи хэл" 
  },
  {
    "title": "Upstream pre intermediate B2student's book",
    "isbn": "978-1-84558-761-1",
    "pageCount": 416,
    "publishedDate": { "$date": "2008-04-01" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Virginia Evans - Jenny Dooley",
    "categories": "Англи хэл" 
  },

{
  "title": "Upstream pre intermediate B2+ student's book",
  "isbn": "978-1-84558-761-1",
  "pageCount": 592,
  "publishedDate": { "$date": "2009-01-14" },
  "thumbnailUrl": require('../assets/images/sharka.jpg'),
  "authors": "Virginia Evans - Jenny Dooley",
  "categories":  "Англи хэл" 
},
  {
    "title": "Head First Javascript",
    "isbn": "1617290084",
    "pageCount": 814,
    "publishedDate": { "$date": "2008-06-03" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": ["Micheal Morrison"],
    "categories": "Програмчилалын хэл"
  },
  {
    "title": "Head First MySQL & PHP",
    "isbn": "1933988746",
    "pageCount": 576,
    "publishedDate": { "$date": "2009-02-02" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Micheal Morrison",
    "categories": "Програмчилалын хэл"
  },
  {
    "title": "Head First Javascript",
    "isbn": "1617290084",
    "pageCount": 621,
    "publishedDate": { "$date": "2008-06-03" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Micheal Morrison",
    "categories": "Програмчилалын хэл"
  },
  {
    "title": "Flex 3 in Action",
    "isbn": "1933988746",
    "pageCount": 576,
    "publishedDate": { "$date": "2009-02-02" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Tariq Ahmed with Jon Hirschi Faisal Abid",
    "categories": "Програмчилалын хэл"
  },
  {
    "title": "Flex 4 in Action",
    "isbn": "1935182420",
    "pageCount": 600,
    "publishedDate": { "$date": "2010-11-15" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Tariq Ahmed ",
    "categories": "Гадаад хэл дээрх ном'"
  },
  {
    "title": "Collective Intelligence in Action",
    "isbn": "1933988312",
    "pageCount": 425,
    "publishedDate": { "$date": "2008-10-01T00:00:00.000-0700" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Satnam Alag",
    "categories": "Гадаад хэл дээрх ном"
  },
  {
    "title": "Zend Framework in Action",
    "isbn": "1933988320",
    "pageCount": 432,
    "publishedDate": { "$date": "2008-12-01T00:00:00.000-0800" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "categories": "Гадаад хэл дээрх ном'"
  },
  {
    "title": "jQuery in Action, Second Edition",
    "isbn": "1935182323",
    "pageCount": 488,
    "publishedDate": { "$date": "2010-06-01T00:00:00.000-0700" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Bear Bibeault Yehuda Katz",
    "categories": "Монгол хэл дээрх ном"
  },
  {
    "title": "Building Secure and Reliable Network Applications",
    "isbn": "1884777295",
    "pageCount": 591,
    "publishedDate": { "$date": "1996-01-01T00:00:00.000-0800" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "authors": "Kenneth P. Birman",
    "categories": "Монгол хэл дээрх ном"
  },
  {
    "title": "Ruby for Rails",
    "isbn": "1932394699",
    "pageCount": 532,
    "publishedDate": { "$date": "2006-05-01T00:00:00.000-0700" },
    "thumbnailUrl": "./assets/images/halzka.jpg",
    "shortDescription": "The word is out: with Ruby on Rails you can build powerful Web applications easily and quickly! And just like the Rails framework itself, Rails applications are Ruby programs. That means you can   t tap into the full power of Rails unless you master the Ruby language.",
    "authors": "David A. Black",
    "categories": "Монгол хэл дээрх ном"
  },
  {
    "title": "The Well-Grounded Rubyist",
    "isbn": "1933988657",
    "pageCount": 520,
    "publishedDate": { "$date": "2009-04-01T00:00:00.000-0700" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "shortDescription": "What would appear to be the most complex topic of the book is in fact surprisingly easy to assimilate, and one realizes that the efforts of the author to gradually lead us to a sufficient knowledge of Ruby in order to tackle without pain the most difficult subjects, bears its fruit.       Eric Grimois, Developpez.com",
    "authors": "David A. Black",
    "categories": "Монгол хэл дээрх ном"
  },
  {
    "title": "Website Owner's Manual",
    "isbn": "1933988452",
    "pageCount": 296,
    "publishedDate": { "$date": "2009-10-01T00:00:00.000-0700" },
    "thumbnailUrl": require('../assets/images/sharka.jpg'),
    "shortDescription": "Website Owner's Manual helps you form a vision for your site, guides you through the process of selecting a web design agency, and gives you enough background information to make intelligent decisions throughout the development process. This book provides a jargon-free overview of web design, including accessibility, usability, online marketing, and web development techniques. You'll gain a practical understanding of the technologies, processes, and ideas that drive a successful website.",
    "authors": "Paul A. Boag",
    "categories": "Монгол хэл дээрх ном"
  }
]
  