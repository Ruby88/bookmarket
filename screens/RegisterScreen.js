import React, { Component } from 'react';
import { View, SafeAreaView ,Text, StyleSheet,KeyboardAvoidingView, RecyclerViewBackedScrollViewComponent } from 'react-native';
import { Headline } from 'react-native-paper';
import { Button } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';
import { apisAreAvailable } from 'expo';
import backend from '../apiHolbolt'






export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        username : '',
        email : '',
        mobile : '',
        password : ''
    };
  }

  render() {
    const { navigation} = this.props;
    let {username, email, mobile, password} =this.state;
    return (
          <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <Headline style={{textAlign : 'center', marginBottom: 30,}}>Та доорх формыг бөглөнө үү!</Headline>
            <TextInput
                label='Нэр'
                mode='outlined'
                value={this.state.username}
                onChangeText={text => this.setState({ username : text})}
            />
                <TextInput
                label='Имэйл хаягаа оруулна уу'
                mode='outlined'
                value={this.state.email}
                onChangeText={text => this.setState({ email : text })}
            />
                <TextInput
                label='Утасны дугаараа оруулна уу'
                mode='outlined'
                value={this.state.mobile}
                onChangeText={text => this.setState({ mobile : text })}
            />
               <TextInput
              label='Нууц үгээ оруулна уу'
              mode='outlined'
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={text => this.setState({ password : text })}
          />
           <View style={{flexDirection : 'row', marginTop : 15}}>
            <Button mode='contained' style={{flex :1, margin : 5,backgroundColor : "#337AFF", }} onPress={()=>{
                navigation.navigate('Welcome')
            }}>
               <Ionicons
                name='md-arrow-round-back'
                size={12}
                style={{ marginRight: 15 }}
                />  Буцах
            </Button>
            <Button mode='contained' style={{flex :1, margin : 5,backgroundColor : "#337AFF", }}  onPress={()=>{

                if (!username && !email && !mobile && !password) {
                  return alert('Та гүйцэт бөглөнө үү!!!')
                } 

              if (mobile.length!==8) {
                return alert('Та буруу дугаар оруулсан байна!!!')
              }
              if (email.indexOf('@') === -1) {
                return alert('Та имэйл хаягаа шалгана уу!!!')
              }

              if (password.length<6) {
                return alert('Та нууц үгээ 6 болон түүнээс дээш тэмдэгт оруулна уу!')
              }


              if (username && email && mobile) {
                // urt code boloh uchiraas external filees duudahaar hiiy
                 backend.create({username, email, mobile,password}, (hariu)=>{
                  if (hariu.isCreated==1) {
                    alert(username + ' Та амжилттай бүртгүүллээ')
                      navigation.navigate('Home', {profile : hariu.body});
                   } else {
                     alert(hariu.message);
                   }
                 });
              
               } 
             
                 

            }}>Бүртгүүлэх </Button>
           </View>

      </KeyboardAvoidingView>
    );
  }
}

RegisterScreen.navigationOptions = {
  title: 'Profile',
};


 const styles = StyleSheet.create({
   container : {
    //  backgroundColor : 'grey',
     flex : 1,
     justifyContent : 'center',
     margin : 20
   },
   buttonContainer : {
    textAlign : 'center',
    flex : 1,
    flexDirection : 'column'
   }
 });


