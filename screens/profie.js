import React, { Component } from 'react';
import { View, Text, StyleSheet,Image, KeyboardAvoidingView,TouchableOpacity } from 'react-native';
import { Headline, Avatar,  Button, TextInput } from 'react-native-paper';
import api from '../apiHolbolt';
import { List } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation} = this.props;
    let {username, password,image} = this.state;
    let profile = navigation.getParam('profile');
    console.warn(profile)

    return( 
      <View>
           <Camera style={{ flex: 1 }} type={Camera.Constants.Type.front} >
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              flex: 0.1,
              alignSelf: 'flex-end',
              alignItems: 'center',
            }}
            onPress={() => {
            
            }}>
            <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip </Text>
          </TouchableOpacity>
        </View>
      </Camera>
        <View style={{alignItems : 'center', marginTop : 20}}>
            <Avatar.Image size={100} source={image || require('../assets/images/avatar.png')} />
            <View style={{flexDirection: 'row'}}>
            <Button onPress={this._pickImage} style={{fontSize : 2}}>+ Gallery</Button>
            <Button onPress={this._pickCamera} style={{fontSize : 2}}>+ Selfie</Button>
            </View>
            <Text style={{fontSize: 30}}>{profile.username || 'Миний нэр'} </Text>

        </View>
        <List.Item
          title="Намайг илэрхийлэх үг"
          description={profile.username || "Item description"}
          left={props => <List.Icon {...props} icon="" />}
        />
        <List.Item
          title="Имэйл хаяг"
          description={profile.email || "Item description"}
          left={props => <List.Icon {...props} icon="email" />}
        />
        <List.Item
          title="Утасны дугаар"
          description={profile.mobile || "Item description"}
          left={props => <List.Icon {...props} icon="contacts" />}
        />
        <List.Item
          title="Бусад"
          description="Item description"
          left={props => <List.Icon {...props} icon="archive" />}
        />
     
      </View>
    );
  }


  componentDidMount() {
    this.getPermissionAsync();
    console.log('hi');
  }

  getPermissionAsync = async () => {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const { status2 } = await Camera.requestPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    });

    console.warn(result);

    if (!result.cancelled) {
      this.setState({ image: {uri : result.uri} });
    }
  };


  _pickCamera = async () =>  {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    })
    console.warn(result);
  
      if (!result.cancelled) {
        this.setState({ image: {uri : result.uri} });
      }
  }
}



ProfileScreen.navigationOptions = {
  title: 'Profile',
};


 const styles = StyleSheet.create({
   container : {
     flex :1,
    //  backgroundColor : '#dcdcdc',
     justifyContent : 'center',
     alignItems : 'center'
   },
   buttonContainer : {
    textAlign : 'center',
    marginTop : 50,
    flexDirection : 'row'
   }
 });