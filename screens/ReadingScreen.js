import React from 'react'
import { StyleSheet, SafeAreaView, View , Text} from 'react-native'
import { WebView } from 'react-native-webview';
import {Button} from 'react-native-paper';

export default class App extends React.Component {

  render() {
    let pdfUrl = this.props.navigation.getParams('pdf');
    return (
      <SafeAreaView style={styles.container}>
        <Button onPress={()=>this.props.navigation.navigate('SettingsStack')} >Буцах</Button>
           {/* <WebView source={{ uri: 'https://archive.org/stream/tourofworldineig00vernrich?ref=ol#page/n6/mode/2up' }} /> */}
           <WebView source={{ uri: pdfUrl }} />
</SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
})