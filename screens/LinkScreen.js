
import { ScrollView, StyleSheet, View, Text, Image } from 'react-native';
import bookList from './nomuud';
import Book from '../components/book'

import React, { Component } from 'react';

export default class Bookmarket extends Component {
  render() {
  let anglil = ['Англи хэл', 'Програмчилалын хэл', 'Гадаад хэл дээрх ном', 'Монгол хэл дээрх ном'];
  let {haihUg, navigation}= this.props
  
    
  return (
    <ScrollView style={styles.container}>
    <Text style={{color: '#000'}}>Олдсон номын тоо {bookList.filter(book=>{
              return book.title.indexOf(haihUg)>-1
            }).length}</Text>
        
      {anglil.map(utga=>{
      return <><Text style={{fontSize: 24, fontWeight: 'bold', color: '#000' ,marginTop: 10, marginBottom: 15,}} >{utga}</Text>
            <ScrollView horizontal={true}>
            {bookList.filter(book=>{ 
                if (!haihUg) return true
                
               return book.title.toLowerCase().indexOf(haihUg.toLowerCase())>-1 || book.authors && book.authors.indexOf(haihUg.toLowerCase()) > -1
            }).map((book, i)=>{
              if (book.categories === utga)
              return <Book data={book} navigation={navigation}  style={{flex:1}} count={book.pageCount}   />
              
            })}
          </ScrollView>
      </>
      
    

      })}

    </ScrollView>
  );
  }
}


Bookmarket.navigationOptions = {
  title: 'Book market',
};

const styles = StyleSheet.create({
  container: {
    flex : 1,
    paddingTop: 15,
    backgroundColor: '#f3f3f3',
  },
  book : {
    flex: 1,
    height : 200,
    margin : 10,
    borderColor: 'brown',
    borderWidth : 2,
  }
});
