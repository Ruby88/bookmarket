import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator} from 'react-navigation-tabs';


import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/UserScreen';
import LinksScreen from '../screens/LinkScreen';
import SettingsScreen from '../screens/SettingsScreen';
import SearchScreen from '../screens/SearchScreen';
import ReadingScreen from '../screens/ReadingScreen';
import ProfileScreen from '../screens/profie' 

import Itbooks from '../screens/Itbooks';

const config = Platform.select({
  // web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: ProfileScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name="md-contacts"
    />
  ),
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
  {
    Links: SettingsScreen,
  },
  config
);

LinksStack.navigationOptions = {
  tabBarLabel: 'Library',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'} />
  ),
};

LinksStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SearchScreen,
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Bookmarket',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-cart' : 'md-cart'} />
  ),
};

// Search tabiin code

const SearchStack = createStackNavigator(
  {
    Settings: Itbooks,
  },
  config
);

SearchStack.navigationOptions = {
  tabBarLabel: 'IT Book',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'md-bookmarks' : 'md-bookmarks'} />
  ),
};


const tabNavigator = createBottomTabNavigator({
  HomeStack,
  LinksStack,
  SettingsStack,
  SearchStack
});

tabNavigator.path = '';

export default tabNavigator;
