import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {Platform} from 'react-native';
import ReadingScreen from '../screens/ReadingScreen';

import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';


import MainTabNavigator from './MainTabNavigator';
import UserScreen from '../screens/UserScreen';
import RegisterScreen from '../screens/RegisterScreen';
const config = Platform.select({
  // web: { headerMode: 'screen' },
  default: {},
});

const ReadingStack = createStackNavigator(
  {
    Reading: ReadingScreen,
  }, 
  config
);
ReadingStack.path = 'Reading';

export default createAppContainer(
  createAnimatedSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Welcome : UserScreen,
    Register : RegisterScreen,
    Main: MainTabNavigator,
    Reading : ReadingScreen
  },
  {
    // The previous screen will slide to the bottom while the next screen will fade in
    transition: (
      <Transition.Together>
        <Transition.Out
          type="slide-right"
          durationMs={400}
          interpolation="easeIn"
        />
        <Transition.In type="fade" durationMs={400} />
      </Transition.Together>
    ),
    transitionViewStyle: { backgroundColor: 'white' },
 
  }));
