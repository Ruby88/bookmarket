import React, { Component } from 'react';
import { View, Text ,StyleSheet, Image,TouchableHighlight, Linking} from 'react-native';
import { SwitchActions } from 'react-navigation';


export default class Book extends Component {
  

  render() {
    let book = this.props.data;
    let huudasniiToo = this.props.count;
  
    return (
        <View style={styles.book} >
                <Text  style={{flex:1, color : '#000', textAlign : 'center'}}>Зохиолч : {book.authors}</Text>
                <Text key={book.isbn} style={{color : '#000', textAlign : 'center'}}>{book.title}</Text>
    
          <TouchableHighlight style={{flexDirection: 'row'}} onPress={()=> {
                  this.props.navigation.navigate('Reading') 
           }}>
              
            <Image  style={{flex:1, height: 300}} source={ book.thumbnailUrl}></Image>
            
           
          
          </TouchableHighlight>
          <Text   style={{textAlign:'center', color:'#000'}}> Хуудас : {huudasniiToo}</Text>
          <Text  style={{textAlign:'center', color:'#000'}}>Хэвлэгдсэн он : {book.publishedDate? book.publishedDate.$date.substring(0,4) : ''}</Text>
         </View>
    );
  }
}

const styles = StyleSheet.create({

    book : {
      margin : 5,
      borderRadius : 10,
      width : 200,
      borderColor: '#E9E6E6',
      borderWidth : 2,
    }
  });
  